package threadedCrawl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;




public class Engine {
	
	private static Test test;
	private Runnable producer;	 

 
	private Set<String> pagesVisited = new HashSet<String>(20);
	private List<String> pagesToVisit = new LinkedList<String>();
  
  //private ObservableList<Website> websiteData = FXCollections.observableArrayList();
  //hashmap where websites are stored + occurrences
  public SortedMap<String, Integer> site = new TreeMap<String, Integer>();
  
  public void start(String url)
  {
	  //for (hasNextURL)
      while(this.pagesVisited.size() < 20)
      {
          String currentUrl;
          Crawler crawler = new Crawler();
          
          if(this.pagesToVisit.isEmpty()){
              currentUrl = url;
              this.pagesVisited.add(url);
              site.put(currentUrl, new Integer(1));
             
          }else{
              currentUrl = this.nextUrl();
          }
          crawler.crawl(currentUrl); // crawl for other <a href> on current site
                                 
          this.pagesToVisit.addAll(crawler.getLinks());
          
      }
      //if max pages visited is reached
      System.out.println("\n**Done** Visited " + this.pagesVisited.size() + " web page(s)");
  }

  
  

  /**
   * Returns the next URL to visit (in the order that they were found). We also do a check to make
   * sure this method doesn't return a URL that has already been visited.
   * 
   * and a check if in hashmap, for displaying the sites and occurrences
   */
  private String nextUrl()
  {
      String nextUrl;
      int count;
      
      //get the entries & iterate over all entries 
      Set set = site.entrySet();
      Iterator iterator = set.iterator();
      
      //display elements
      while(iterator.hasNext()){
		Map.Entry me = (Map.Entry)iterator.next();
		System.out.print("site: " + me.getKey() + ": " + me.getValue() + "\n");
      }
      
      //prints out every site, visited
      //pagesVisited.forEach(System.out::println);
      
      
    
      
      
      //if in pagesVisited remove from pages to visit
      do{
          nextUrl = this.pagesToVisit.remove(0);
          //check if in hashmap increment occurence
          if(site.containsKey(nextUrl) == true){
        	  //get observer here if changes then notify 
        	  site.put(nextUrl, site.get(nextUrl)+1);	
              System.out.println("**New Occurrence for " + nextUrl + ", occ:" +
    								site.get(nextUrl));
          }else{
        	  site.put(nextUrl, new Integer(1));
    		  System.out.println("**next URL: "+nextUrl + ": " + site.get(nextUrl));
    	  }
      } while(this.pagesVisited.contains(nextUrl));
      this.pagesVisited.add(nextUrl);
      
      
      
      return nextUrl;
  }




}
