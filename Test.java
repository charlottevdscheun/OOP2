package threadedCrawl;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {  
	    public static void main(String args[]){
	    
	    Engine engine = new Engine();
	    //capacity of the queue
	    int capacity = 10;
	    BlockingQueue<String> list = new ArrayBlockingQueue<String>(capacity);
	    //threadpool of 3
	    final ExecutorService executor = Executors.newFixedThreadPool(3);
	    
	    
	    
	    Runnable producer;
	    producer = new Runnable(){
	    	public void run(){
	    		try{
	    			String testSite = "https://facebook.com";
	    			list.put(testSite);
	    			System.out.println("putting: " + testSite);
	    			engine.start(testSite);
	    		}catch(InterruptedException ie){
	    			assert false;
	    			System.out.println("error producer");
	    		}
	    	}
	    };
	    executor.execute(producer);
	    
	    Runnable consumer;
	    consumer = new Runnable(){
	    	public void run(){
	    		try{
	    			String url = list.take();

	    			System.out.println("getting: " + url);
	    			
	    		}catch(InterruptedException ie){
	    			System.out.println("error consumer");
	    		}
	    		executor.shutdownNow();
	    	}
	    };
	    executor.execute(consumer);
	
	}
}
